<?php

return [
    'index_success' => 'Записи успешно получены.',
    'store_success' => 'Создана новая запись.',
    'update_success' => 'Запись успешно обновлена.',
    'delete_success' => 'Запись успешно удалена.',

    'delete_category_error' => 'Указанная категория имеет привязанные товары.'
];
