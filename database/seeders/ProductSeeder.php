<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::factory()->count(100)->create()->each(function (Product $product) {
            $product->categories()->sync(Category::all()->random(mt_rand(2, 10)));
        });
    }
}
