<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->words(mt_rand(1, 3), true),
            'is_published' => $this->faker->boolean,
            'price' => $this->faker->randomFloat(null, 0, 100000)
        ];
    }
}
