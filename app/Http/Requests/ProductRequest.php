<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->isMethod('POST')) {
            return [
                'name' => 'required|string|max:255',
                'price' => 'required|numeric|min:0',
                'is_published' => 'required|boolean',
                'categories' => 'required|array|min:2|max:10',
                'categories.*' => 'int|distinct|exists:categories,id'
            ];
        } else {
            return [
                'name' => 'string|max:255',
                'price' => 'numeric|min:0',
                'is_published' => 'boolean',
                'categories' => 'array|min:2|max:10',
                'categories.*' => 'int|distinct|exists:categories,id'
            ];
        }
    }
}
