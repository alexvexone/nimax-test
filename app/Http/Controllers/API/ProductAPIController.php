<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\ProductRequest;
use App\Models\Product;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;

class ProductAPIController extends APIController
{
    /**
     * Получить список товаров.
     *
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $request->validate([
            'name' => 'string',
            'price' => 'string',
            'is_published' => 'boolean',
            'is_deleted' => 'boolean',
            'category_id' => 'int',
            'category_name' => 'string',
        ]);

        $products = Product::withTrashed();

        if ($request->has('name')) {
            $products->where('name', 'like', '%' . $request->get('name') . '%');
        }

        if ($request->has('price')) {
            $array = explode(',', $request->get('price'));
            $from = Arr::first($array);
            $to = Arr::get($array, 1);

            $products->where('price', '>=', $from);

            if (!is_null($to)) {
                $products->where('price', '<=', $to);
            }
        }

        if ($request->has('is_published')) {
            $products->where('is_published', $request->get('is_published'));
        }

        if ($request->has('is_deleted')) {
            if ($request->get('is_deleted')) {
                $products->whereNotNull('deleted_at');
            } else {
                $products->whereNull('deleted_at');
            }
        }

        if ($request->has('category_id')) {
            $products->whereRelation('categories', 'id', $request->get('category_id'));
        }

        if ($request->has('category_name')) {
            $products->whereRelation('categories', 'name', 'like', '%' . $request->get('category_name') . '%');
        }

        return $this->sendSuccess(__('rest.index_success'), $products->get());
    }

    /**
     * Создать новый товар.
     *
     * @param  ProductRequest  $request
     *
     * @return JsonResponse
     */
    public function store(ProductRequest $request): JsonResponse
    {
        $validatedData = $request->validated();
        $product = Product::create(Arr::except($validatedData, 'categories'));
        $product->categories()->sync(Arr::get($validatedData, 'categories'));

        return $this->sendSuccess(__('rest.store_success'), $product, Response::HTTP_CREATED);
    }

    /**
     * Обновить указанный товар.
     *
     * @param  ProductRequest  $request
     * @param  Product  $product
     *
     * @return JsonResponse
     */
    public function update(ProductRequest $request, Product $product): JsonResponse
    {
        $validatedData = $request->validated();
        $product->update(Arr::except($validatedData, 'categories'));
        $product->categories()->sync(Arr::get($validatedData, 'categories'));

        return $this->sendSuccess(__('rest.update_success'), $product);
    }

    /**
     * Удалить указанный товар.
     *
     * @param  Product  $product
     *
     * @return JsonResponse
     */
    public function destroy(Product $product): JsonResponse
    {
        $product->delete();
        return $this->sendSuccess(__('rest.delete_success'));
    }
}
