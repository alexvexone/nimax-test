<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class CategoryAPIController extends APIController
{
    /**
     * Создать новую категорию.
     *
     * @param  CategoryRequest  $request
     *
     * @return JsonResponse
     */
    public function store(CategoryRequest $request): JsonResponse
    {
        return $this->sendSuccess(__('rest.store_success'), Category::create($request->validated()), Response::HTTP_CREATED);
    }

    /**
     * Удалить указанную категорию.
     *
     * @param  Category  $category
     *
     * @return JsonResponse
     */
    public function destroy(Category $category): JsonResponse
    {
        if ($category->products()->exists()) {
            return $this->sendError(__('rest.delete_category_error'), Response::HTTP_BAD_REQUEST);
        }

        $category->delete();
        return $this->sendSuccess(__('rest.delete_success'));
    }
}
